1. Get your personal API token from https://api.wikimedia.org/wiki/Special:AppManagement and copy its contents into `.token` in this directory.
2. Get a line-separated list of page IDs and copy it into `pageids.txt` in this directory
3. `php app.php`
