<?php

$file = 'pageids.txt';
$pageIds = array_filter( explode( "\n", file_get_contents( $file ) ) );
$token = file_get_contents( '.token' );

$opts = [
	'http' => [
		'method' => 'GET',
		'header' => 'Authorization: Bearer ' . $token
	]
];

$context = stream_context_create( $opts );
$apiUrl = 'https://api.wikimedia.org/core/v1/wikipedia/es/search/page?q=';
$query = 'hasrecommendation:image -hastemplatecollection:infobox pageid:%d';
$found = 0;
$notFound = 0;
$batch = 50;
$total = count( $pageIds );
echo "Processing $total items...\n";
$count = 0;
foreach ( $pageIds as $key => $pageId ) {
	$count++;
	if ( $count % $batch === 0 ) {
		echo "== Processed $count out of $total ==\n";
	}
	echo( "Checking $pageId..." );
	$url = $apiUrl . urlencode( sprintf( $query, (int)$pageId ) );
	$result = json_decode( file_get_contents( $url, false, $context ), true );
	echo( " image recommendation " );
	if ( isset( $result['pages'] ) && count( $result['pages'] ) ) {
		echo "found\n";
		$found++;
	} else {
		echo "not found\n";
		$notFound++;
	}
	echo "..... $url\n";
}
echo "$found articles with image recommendations found; $notFound not found";
